## Day Planner

### 1. Introduction
- [ ] 10:00 welcome & introduction
- [ ] 10:02 about me
- [ ] 10:05 introduction to the 3 fields
- [ ] 10:10 field 1: speculative design
- [ ] 10:15 physical prototyping
- [ ] 10:20 e-textiles & wearable tech

### 2. Physical Prototyping
- [ ] 10:30 hi microbit
- [ ] 10:35 electronics
- [ ] 10:45 general intro
- [ ] 11:10 tinkercad
- [ ] 11:12 where to take this from here...

### 15 min break
- [ ] 11:15 15 min break 

### 3. Design Thinking + Speculative Design
- [ ] 11:30 brainstorm whatif in pairs
- [ ] 11:40 diamond explained as a process
- [ ] 11:42 zoom into the world
- [ ] 11:45 work on it in groups
- [ ] 11:50 group ideas
- [ ] 11:55 dotmocracy
- [ ] 12:00 work on 4 outcomes
			- concept: visualize (5')
			- prototype (virtual) hardware & software
			- learnings / reflections
- [ ] 12:30 presentations

### 4. Wrap Up
- [ ] 12:50 feedback