---
theme: black
highlightTheme: monokai
// css: layout.css
---
# welcome
---
[how are you doing?]
---
![[Pasted image 20221021195255.png]]
---
Ann Peeters \
(she/her) \
ann.peeters.email@gmail.com

![[Pasted image 20221021214229.png|]]
_educational e-textiles tools_, Ann Peeters and Claire Williams - 2020

note:
- being a boy: unfair > knitting for girls, drawing for boys
- my mom is very good at it
- but I'm not > crafts is loosing terrain compared to previous generations ?
How is that in the Philippines?

---
![[Three-set Venn diagram - Color (2).png]]


note: 
- Speculative design: in the real of Futures Studies & Design Thinking
- Physical Prototyping : software on embedded systems, interaction with the real world
- wearable tech & e-textiles : crossover between electronics & fabrics, between fashion & future

---
# Futures 

![[Pasted image 20221022084239.png]]
Can you spot a link with `improbable futures` ?


note: 
- why do things tell us something about the future ?
- Almost all geochemists believe petroleum results from a few million years of decay of once-living organisms
- petroleum is made into plastics
- btw. did you know fleece, polyester, is a systhetic fibre? and thus platic...


---
## Design Fiction
![[Pasted image 20221021204829.png]]
<author>_Transfigurations_, Agi Haines</author> - 
2016
note:
- narrative, story, script, imagination, fantasy
- provocative questions: `what if`
- We don't play by the rules:
	- NOT: teachnically feasable
	- NOT: what economic model
- helps to have `artefacts` to get interaction

A tool to enlarge our capacity of wonder a narrative device to materilize our imagination as a collective. Speculative design allows us to use **provocation to encourage debate** more openly. We generate questions around each project beginning with « what if ». Create scenarios so people can start understanding the values and ethics embedded in design of objects, products and systems and imagine new ways of existing. Design Fiction interogate the esthetiques of contemporary electronic objects. Not only to create a different object but also make him integrate in a history and illustrate how he is used. Imagine a world in which this object inscribes itself and illustrate how it is used. Imagien a world in which this object can be used and interogates our everyday lives and the practises of design that shapes it. These objects can then be used to create debates to which futures we tend. As science fiction does this can allow us to present it’s alternatives.

---
## Physical Prototyping
![[Pasted image 20221021212035.png]]
Unknown maker

Note:
**thing**
- visual, tangeable
- it represents your vision/concept/idea
- also used to get early user-input about a design

 **process** 
 - lots of iterations, research
 - 'fail fast'
 - low fidelity (high 'pretend as if..' factor)
 - can be on paper
 
 **play**
 - childlike materials
 - simpel, quick
 - you can learn from it
 - sandbox

---
## What are wearables ?

note:
What are wearables ?
- what about a facemask? glasses? watch? jewelery? hat?
- what about makeup ?
- body altering ? / cyborg ?
- wat about material research on eg. vegan leather

---
Why do we wear?
1. express 
2. communicate
3. protect

note: 
- Could you give examples?
- can NOT wearing something mean something?
	- "the personal is political" : headscarf in Iran, NOT wearing some
- my partner has never worn a hoodie before, but with the energy prices going up, we don't warm our houses as much >> adapted clothing
- prepping protesters

---
## Wearables
![[Pasted image 20221021212809.png]]
Ikeuchi Hiroto - 2018

note:
- technological revolutions that are there, but haven't gotten a breakthrough (=/= reasons) eg. bone conductivity
- artists/designers : don't make it for the real world
- it says something about our world...
- and there is a lot to say

---
![[Pasted image 20221022015951.png]]

---
![[Pasted image 20221022020039.png]]

---

![[Pasted image 20221022020101.png]]
---

![[Pasted image 20221022020134.png]]

---
![[Pasted image 20221022020203.png]]

---
![[Pasted image 20221022020233.png]]

---
## Some iconic examples...

note:
when | what
---- | ----
very far back | women's (home)work, crafts
1600 | conductive thread
... | ...

much a women's story
importance of crafts: weaving, hand stitching,...

---

---
![[Pasted image 20221021220351.png]]
_Electric Dress_,  Atsuko Tanaka - 1950

note:
arts

---
![[Pasted image 20221021220423.png]]
Diana Dew - 1960s

note: 
- fashion
- link ? timeframe ? commercializing ?

---

![[bead_2_detail_3_movements_edit_3.gif]]
_The Embroidered Computer_, Irene Posch - 2018

note:
you should recognise this...

---

![[Pasted image 20221022083322.png]]
Magnetic core memory plane, from : Computer History Museum

note:
- semiconductors (small chips) made this technique obsolete

---
![[photo-by-mark-richards-computer-history-museum.webp]]
Rope memory from the Apollo Guidance Computer

note:
soft electronics
woven

---
![[Pasted image 20221021222836.png]]
_“I am Very Happy I Hope You Are Too”_, Shih Wei Chieh - 2013

note:
Jake Ramos tied it to traditional clothing, and he is not the only one...
- emotions
- memories
- traditions

---

![[Pasted image 20221022013907.png]]
_Climate Dress_, Difus Design - 2009

note:
- "predicting" notion of climate dress - glimpses of the future...
- led reflect the CO2

---
![[Pasted image 20221021223528.png]]

_The Crying Dress_ , Mika Satomi & Hannah Perner-Wilson (aka Kobakant) - 2012

note:
"In 2020, the global economy is in shambles. Localized production and craftsmanship experiences it’s first major boom since it’s decline preceding the 1920′s Industrial Revolution. The electronics sector in particular is hit by a scarcity of resources, causing prices to skyrocket. Electronics, previously known for their uniformity and quantity, almost overnight become a showcase for individuality, materiality and skilled labor. This development is now commonly known as Exquisite Electronics and today we live in a world where the rich commission extravagant Haute Couture electronics reminiscent of pre-industrial eras."

---
## Fabricademy
-   [2. Digital Bodies](http://fabricademy.fabcloud.io/handbook/classes/02_digital_human/ "2. Digital Bodies")
-   [3. Open Source Circular Fashion](http://fabricademy.fabcloud.io/handbook/classes/03_circular_fashion/ "3. Open Source Circular Fashion")
-   [4. Biochromes](http://fabricademy.fabcloud.io/handbook/classes/04_biofabrics/ "4. Biochromes")
-   [5. E-textiles](http://fabricademy.fabcloud.io/handbook/classes/05_etextiles/ "5. E-textiles")
-   [6. Biofabricating Materials](http://fabricademy.fabcloud.io/handbook/classes/06_biofabricating/ "6. Biofabricating Materials")
-   [7. Open source Hardware- from fibers to fabric](http://fabricademy.fabcloud.io/handbook/classes/07_open_source_hardware/ "7. Open source Hardware- from fibers to fabric")
-   [8. Computational Couture](http://fabricademy.fabcloud.io/handbook/classes/08_computational/ "8. Computational Couture")
-   [9. Textile as Scaffold](http://fabricademy.fabcloud.io/handbook/classes/09_textile_scaffold/ "9. Textile as Scaffold")
-   [10. Wearables](http://fabricademy.fabcloud.io/handbook/classes/10_wearables/ "10. Wearables")
-   [11. Implications and applications](http://fabricademy.fabcloud.io/handbook/classes/11_applications/ "11. Implications and applications")
-   [12. Soft Robotics](http://fabricademy.fabcloud.io/handbook/classes/12_softrobotics/ "12. Soft Robotics")
-   [13. Skin electronics](http://fabricademy.fabcloud.io/handbook/classes/13_skinelectronics/ "13. Skin electronics")

note:
- there is so much more to tell...
- all topics to explore
- website 2017 has more materials online
- lots of student projects
---


---
# Electronics


---

input -> computer -> output

---

some components: 
- breadboard
- led
- resistor
- cables
- microbit

---

![[Pasted image 20221022090618.png]]

---

![[Pasted image 20221022090258.png]]
LED

---

![[Pasted image 20221022090942.png]]

---

inputs
1. digital sensors ( -> cf. radio on / off)
1. analog sensors (-> cf. volume knob)

note:

- give slides to get you started
- https://docs.google.com/presentation/d/1vHcWzdX4o68nrXiT6MbqrNqVz2XUmLi19uJolQ7xnfA/edit#slide=id.g47fac6d7f6_0_195
- 
---
[Hello world, BBC:Microbit](https://microbit.org/get-started/first-steps/radio-and-pins/)\
[Hello Python ](https://python.microbit.org/v/3)

note:
- questions ?
- all ok with the environment?
- radio communication
- yes, you can work in python for your project (for now, keep Blocks? I don't want you to construct an elaborate piece)

---

[Hi, TinkerCAD Circuits](https://www.tinkercad.com/dashboard)

---
[How to get what you want](https://www.kobakant.at/DIY/?cat=26) \
Kobakant

note:
THE research blog on e-textiles

---
https://etextile-summercamp.org/swatch-exchange/media/
![[Pasted image 20221022015338.png]]
note:
- the orgiginal exchange
- lots more women
- would the world looked have differently ?

---

# BREAK

---

![[Pasted image 20221022022815.png]]

---

![[Pasted image 20221022022957.png]]

---

![[Pasted image 20221022023032.png]]

---

Let's move to Miro

note:

We are far far into the future:
- pick a profession
- pick a location (top of a mountain, alone in the jungle, in space,... )
- pick a broad topic that concerns you: way of living, eating, ways to communicate, rituals,...
- pick a future: dystopian, utopian, heterotopian
- pick a group you belong to : power, rebelious, 'poor', researchers, designers,...
- pick a place on the body

---
![[Pasted image 20221022020507.png]]

---
Thank you

Questions?

---
