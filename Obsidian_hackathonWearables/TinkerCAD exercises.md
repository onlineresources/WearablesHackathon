# Using Micro:bit With External Components
![[Screenshot 2022-10-24 at 09.42.05.png]]

20 min
hands-on
electronics


Make an account on Tinkercad (Autodesk). https://www.tinkercad.com
Go to Resources > Learning Center and scroll to 'Learn Circuits' and click 'view all'.![[Screenshot 2022-10-24 at 09.41.29.png]]
- Pick 'Using Micro:bit With External Components'. 
- You will learn how to use a potentiometer and a (small) motor, but it should give you some ideas on how to wire other things as well.
- If you have the kit, try to do it physically with the live components as well.



# Using a Micro:bit with a Breadboard
![[Screenshot 2022-10-24 at 09.59.16.png]]
20 min 
hands-on
electronics 

optional

To review what we did in the workshop on using a breadboard + wiring an LED you can take a look at this lesson in Tinkercad Circuits.

Make an account on Tinkercad (Autodesk). https://www.tinkercad.com
Go to Resources > Learning Center and scroll to 'Learn Circuits' and click 'view all'.![[Screenshot 2022-10-24 at 09.41.29 1.png]]
- Pick 'Using Micro:bit With a Breadboard'. 
- You will review how a breadboard works and wire 1 to 3 LEDs to your Micro:bit.
- If you have the kit, try to do it physically with the live components as well.

